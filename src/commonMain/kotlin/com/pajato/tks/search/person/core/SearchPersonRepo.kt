package com.pajato.tks.search.person.core

import com.pajato.tks.common.core.SearchKey
import com.pajato.tks.pager.core.PageData
import com.pajato.tks.pager.core.PagedResultPerson

public interface SearchPersonRepo {
    public suspend fun getSearchPagePerson(key: SearchKey): PageData<PagedResultPerson>
    public suspend fun getSearchResults(key: SearchKey): List<PagedResultPerson>
}
